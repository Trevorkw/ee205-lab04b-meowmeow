/* mmdecode.h - MeowMeow, a stream encoder/decoder */
//Name:Trevor Chang
//Email: trevorkw@hawaii.edu
//Date: 6 Feb 2022
//Filename: mmdecode.h


#ifndef _MMDECODE_H
#define _MMDECODE_H

#include <stdio.h>

int mm_decode(FILE *src, FILE *dst);

#endif	/* _MMDECODE_H */
