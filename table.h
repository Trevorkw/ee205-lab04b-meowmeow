/* table.h - MeowMeow, a stream encoder/decoder */
//Name:Trevor Chang
//Email: trevorkw@hawaii.edu
//Date: 6 Feb 2022
//Filename: table.h


#ifndef _TABLE_H
#define _TABLE_H

#define ENCODER_INIT { "purr", "purR", "puRr", "puRR", \
		       "pUrr", "pUrR", "pURr", "pURR", \
                       "Purr", "PurR", "PuRr", "PurR", \
		       "PUrr", "PUrR", "PURr", "PURR" }

#endif	/* _TABLE_H */
