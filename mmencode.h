/* mmencode.h - MeowMeow, a stream encoder/decoder */
//Name:Trevor Chang
//Email: trevorkw@hawaii.edu
//Date: 6 Feb 2022
//Filename: mmencode.h


#ifndef _MMENCODE_H
#define _MMENCODE_H

#include <stdio.h>

int mm_encode(FILE *src, FILE *dst);

#endif	/* _MMENCODE_H */
